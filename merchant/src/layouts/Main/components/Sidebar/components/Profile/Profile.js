import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Avatar, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: 'fit-content'
  },
  avatar: {
    width: 100,
    height: 100,
    border: '1.5px solid #616161',
  },
  name: {
    marginTop: theme.spacing(2),
    color: 'white',
    marginBottom: theme.spacing(1),
  },
  status: {
    border: '1.5px solid #616161',
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    borderRadius: 20,
  }
}));

const Profile = props => {
  const { className, ...rest } = props;

  const classes = useStyles();

  const user = {
    name: 'KFC PUSAT',
    avatar: '/images/themes/store-icon.svg',
    bio: 'GOLD MERCHANT'
  };

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Avatar
        alt="Person"
        className={classes.avatar}
        component={RouterLink}
        src={user.avatar}
        to="/settings"
      />
      <Typography
        className={classes.name}
        variant="h4"
      >
        {user.name}
      </Typography>
      <div className={classes.status}>
        <Typography variant="body2">{user.bio}</Typography>
      </div>
    </div>
  );
};

Profile.propTypes = {
  className: PropTypes.string
};

export default Profile;
