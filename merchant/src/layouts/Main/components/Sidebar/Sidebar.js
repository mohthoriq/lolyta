import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Drawer, Typography, Divider } from '@material-ui/core';
import PollIcon from '@material-ui/icons/Poll';
import PublicIcon from '@material-ui/icons/Public';
import SettingsIcon from '@material-ui/icons/Settings';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import MoneyIcon from '@material-ui/icons/Money';
import CardIcon from '@material-ui/icons/CardMembership';
import StoreIcon from '@material-ui/icons/Store';
// import StoreIcon from 'icons/Telkomsel/Store';
import Profile from './components/Profile';

import { SidebarNav } from './components';

const useStyles = makeStyles(theme => ({
  drawer: {
    width: 240,
    [theme.breakpoints.up('lg')]: {
      marginTop: 64,
      height: 'calc(100% - 64px)'
    }
  },
  root: {
    backgroundColor: '#132236',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    paddingTop: theme.spacing(2)
  },
  menuText: { marginLeft: theme.spacing(3), color: '#999999' },
  nav: {
    marginBottom: theme.spacing(2)
  },
  icon: { color: 'gray' },
  divider: {
    color: '#2B384A',
    backgroundColor: '#2B384A',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

const Sidebar = props => {
  const { open, variant, onClose, className, ...rest } = props;

  const classes = useStyles();

  const pages = [
    {
      title: 'Dashboard',
      href: '/dashboard',
      icon: <PublicIcon />
    },
    {
      title: 'My Report',
      href: '/users',
      icon: <PollIcon />
    },
    {
      title: 'My Info',
      href: '/products',
      icon: <StoreIcon />
    },
    {
      title: 'My Outlet',
      href: '/sign-in',
      icon: <LockOpenIcon />
    },
    {
      title: 'My Program',
      href: '/typography',
      icon: <MoneyIcon />
    },
  ];

  return (
    <Drawer
      anchor="left"
      classes={{ paper: classes.drawer }}
      onClose={onClose}
      open={open}
      variant={variant}
    >
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
        <Profile />
        <Divider className={classes.divider} />
        <Typography
          align="left"
          className={classes.menuText}
          variant="h6"
        >
          MAIN MENU
        </Typography>
        <SidebarNav
          className={classes.nav}
          pages={pages}
        />
      </div>
    </Drawer>
  );
};

Sidebar.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired
};

export default Sidebar;
