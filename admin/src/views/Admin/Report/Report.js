import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import {
  Typography,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Select,
  Grid,
  Paper,
  FormControl
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

import { ReportTable } from './components';
import mockData from './data';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  },
  content: {
    marginTop: theme.spacing(4)
  },
  formControl: {
    // marginLeft: 20,
    display: 'flex',
    width: 200
  },
  select: {
    borderWidth: 1,
    borderColor: 'grey',
    width: 100
  }
}));

const UserList = () => {
  const classes = useStyles();

  const [users] = useState(mockData);

  return (
    <div className={classes.root}>
      <Typography variant="h2">Report</Typography>
      <Typography variant="h6">Report</Typography>
      <div className={classes.content}>
        <Card>
          <CardHeader title="Report List" />
          <Divider />
          <CardContent>
            <FormControl
              className={classes.formControl}
              variant="outlined"
            >
              <SearchIcon />
              <Select
                // inputProps={<SearchIcon />}
                className={classes.select}
                disableUnderline
                native
                // onChange={handleChange('age')}
                value={10}
              >
                <option value="" />
                <option value={10}>Ten</option>
                <option value={20}>Twenty</option>
                <option value={30}>Thirty</option>
              </Select>
            </FormControl>
            <ReportTable users={users} />
          </CardContent>
          <Divider />
        </Card>
      </div>
    </div>
  );
};

export default UserList;
